package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    public LoginPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }
    @FindBy(name="UserName")
    private WebElement txtUserName;

    @FindBy(name ="Password")
    private WebElement txtPassword;

    @FindBy(name ="Login")
    private WebElement btnLogin;

    public void Login(String UserName,String Password){
        txtUserName.sendKeys(UserName);
        txtPassword.sendKeys(Password);
    }
    public void ClickLogin(){
        btnLogin.submit();
    }
}
