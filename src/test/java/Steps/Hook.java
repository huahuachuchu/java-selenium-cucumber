package Steps;

import Base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class Hook extends BaseUtil {
    private final BaseUtil base;

    public Hook(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void InitializeTest(){
        //Passing a dummy WebDriver instance
        System.out.println("Opening the browser : MOCK");
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        base.Driver = new ChromeDriver();
        base.Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        base.Driver.manage().window().maximize();
    }
    @After
    public void TearDown(Scenario scenario){
        if(scenario.isFailed()){
            //Take screenshot
            System.out.println(scenario.getName());
        }
        base.Driver.quit();
    }
}
