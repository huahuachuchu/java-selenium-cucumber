package Steps;

import Base.BaseUtil;
import Pages.LoginPage;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class LoginSteps extends BaseUtil {
    private BaseUtil base;

    public LoginSteps(BaseUtil base) {
        this.base = base;
    }

    @Then("^I should see the user page$")
    public void iShouldSeeTheUserPage() throws Throwable{
        Assert.assertEquals(base.Driver.findElement(By.id("Initial")).isDisplayed(),true,"Its not displayed");
    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() throws Throwable {
        System.out.println("Navigate Login Page");
        base.Driver.navigate().to("http://www.executeautomation.com/demosite/Login.html");
    }

    @And("^I click login button$")
    public void  iClickLoginButton() throws Throwable {
        LoginPage page = new LoginPage(base.Driver);
        page.ClickLogin();
    }

    @And("^I enter the following to login$")
    public void iEnterTheFollowingToLogin(DataTable  table) throws Throwable {
        //Create an ArrayList
        List<User> users =new ArrayList<User>();
        //store all the users
        users = table.asList(User.class);
        LoginPage page =new LoginPage(base.Driver);
         for (User user: users){
             page.Login(user.username,user.password);
         }
    }

    @And("^I enter ([^\"]*) and ([^\"]*)$")
    public void iEnterUsernameAndPassword(String username,String password) throws Throwable{
        System.out.println("username is "+ username);
        System.out.println("password is "+ password);
    }


    public class User{
        public String username;
        public String password;

        public User(String username, String password) {
            this.username = username;
            this.password = password;
        }
    }
}
